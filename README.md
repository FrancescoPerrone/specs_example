# XRF UI RESTful Specifications

## Resource: Dataset

### C++ Class: `Dataset`
### Python Class: `DatasetPy`

**Methods/Functions**:

- **loadDataset** 
  - **Input**: `filePath` (String)
  - **Action**: Load dataset from given path.
  - **Return**: `datasetID` (Integer)

- **getDataset**
  - **Input**: `datasetID` (Integer)
  - **Action**: Retrieve dataset details.
  - **Return**: `datasetDetails` (Object)

- **deleteDataset**
  - **Input**: `datasetID` (Integer)
  - **Action**: Remove dataset from application.
  - **Return**: `status` (Boolean)

---

## Resource: Analysis

### C++ Class: `Analysis`
### Python Class: `AnalysisPy`

**Methods/Functions**:

- **runAnalysis**
  - **Input**: `datasetID` (Integer), `analysisParams` (Object)
  - **Action**: Execute XRF analysis.
  - **Return**: `analysisResults` (Object) or `analysisID` (Integer)

- **getAnalysisResults**
  - **Input**: `analysisID` (Integer)
  - **Action**: Retrieve analysis results.
  - **Return**: `analysisResults` (Object)

---

## Resource: Visualization

### C++ Class: `Visualization`
### Python Class: `VisualizationPy`

**Methods/Functions**:

- **createVisualization**
  - **Input**: `datasetID` (Integer), `visParams` (Object)
  - **Action**: Generate visualization.
  - **Return**: `visualizationObject` (Object) or `visualizationID` (Integer)

- **getVisualization**
  - **Input**: `visualizationID` (Integer)
  - **Action**: Retrieve visualization.
  - **Return**: `visualizationObject` (Object)

---

## UML-like Diagram Description:

**Classes**:
- `Dataset`, `DatasetPy`
- `Analysis`, `AnalysisPy`
- `Visualization`, `VisualizationPy`

**Associations**:
1. `Dataset` -> `Analysis`: A dataset can have one or more analyses.
2. `Dataset` -> `Visualization`: A dataset can have one or more visualizations.

**Methods**:
Under each class, list the methods. For example, under `Dataset`, you'd have:
- `loadDataset(filePath)`
- `getDataset(datasetID)`
- `deleteDataset(datasetID)`




# Functional Analysis: User Capabilities for XRF UI

## What the User Can/Might Be Able To Do:

1. **Load Datasets**: The user can load XRF datasets into the application using the `loadDataset` function by providing a file path.
2. **View Dataset Details**: After loading a dataset, the user can view its details using the `getDataset` function, given they have the unique dataset ID.
3. **Delete Datasets**: If a user no longer needs a dataset, they can remove it from the application using the `deleteDataset` function.
4. **Run Analyses on Datasets**: With the loaded datasets, the user can perform XRF analyses by invoking the `runAnalysis` function. This function may require specific parameters related to the analysis.
5. **Retrieve Analysis Results**: Once an analysis is completed, the user can fetch the results using the `getAnalysisResults` function, provided they have the unique analysis ID.
6. **Visualize Datasets**: For better understanding or presentation, the user can create visualizations of their datasets using the `createVisualization` function.
7. **View Created Visualizations**: Any generated visualization can be viewed using the `getVisualization` function as long as the user has its unique ID.

## What the User Would Not Be Able To Do:

1. **Direct Editing of Datasets**: Based on the functions provided, there's no direct method for editing or manipulating the raw dataset once it's loaded into the application.
2. **Save Analysis or Visualization Results**: The current specification doesn't mention functions to save the results of analyses or visualizations to external files.
3. **Share or Collaborate in Real-time**: The toy UI doesn't have functions that would support real-time sharing or collaborative work on datasets, analyses, or visualizations.
4. **Access Historical Data**: The specifications do not mention any functionality for accessing a history or log of previous datasets, analyses, or visualizations.
5. **Customize Visualizations**: While the user can create visualizations, there's no mentioned functionality for customizing the appearance or style of these visualizations.
6. **Batch Processing**: The user might not be able to process multiple datasets or run analyses on multiple datasets simultaneously as there's no function outlined for batch operations.
